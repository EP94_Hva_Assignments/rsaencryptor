﻿using RsaEncryptor.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RsaEncryptor.Interfaces
{
    public interface IRsaEncryptor
    {
        RsaKeyPair GenerateRsaKeyPair();
        string Encrypt(string stringToEncrypt, RsaKey publicKey);
        string Decrypt(string stringToDecrypt, RsaKey privateKey);
    }
}
