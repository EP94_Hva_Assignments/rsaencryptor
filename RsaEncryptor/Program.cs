﻿using RsaEncryptor.Interfaces;
using RsaEncryptor.Models;
using System;

namespace RsaEncryptor
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Starting RSAEncryptor, press Ctrl+c to quit...");
            IRsaEncryptor rsa = new Rsa();
            Console.WriteLine("Start generating RSA Key Pair...");
            RsaKeyPair keyPair = rsa.GenerateRsaKeyPair();
            Console.WriteLine($"Public key: {keyPair.PublicKey.Key}, Private key: {keyPair.PrivateKey.Key}");

            while (true)
            {
                Console.WriteLine("Please enter sentence to encrypt and press enter");
                string sentence = Console.ReadLine();
                string encrypted = rsa.Encrypt(sentence, keyPair.PublicKey);
                Console.WriteLine($"Encrypted: {encrypted}");
                string decrypted = rsa.Decrypt(encrypted, keyPair.PrivateKey);
                Console.WriteLine($"Decrypted: {decrypted}");
            }
        }
    }
}
