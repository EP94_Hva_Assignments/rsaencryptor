﻿using RsaEncryptor.Interfaces;
using RsaEncryptor.Models;
using RsaEncryptor.Utils;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace RsaEncryptor
{
    public class Rsa : IRsaEncryptor
    {
        /// <summary>
        /// Generates a public and private key pair
        /// </summary>
        /// <returns>A public and private key pair</returns>
        public RsaKeyPair GenerateRsaKeyPair()
        {
            long p = GenerateRandomPrimeNumber();
            long q;
            while ((q = GenerateRandomPrimeNumber()) == p) ;
            long n = p * q;
            long k = (p - 1) * (q - 1);
            RsaKey privateKey = new RsaKey() { Product = n, Key = FindDerivedNumber(k) };
            RsaKey publicKey = new RsaKey() { Product = n, Key = FindDecryptionKey(privateKey.Key, Math.Abs(k)) };
            return new RsaKeyPair()
            {
                PublicKey = publicKey,
                PrivateKey = privateKey,
                K = k
            };
        }

        /// <summary>
        /// Decrypt a string using the private key
        /// </summary>
        /// <param name="stringToDecrypt">The string to decrypt</param>
        /// <param name="privateKey">The private key</param>
        /// <returnsThe decrypted string></returns>
        public string Decrypt(string stringToDecrypt, RsaKey privateKey)
        {
            StringBuilder sb = new StringBuilder();
            byte[] bytes = Convert.FromBase64String(stringToDecrypt);
            long[] valuesToDecrypt = bytes.GetLongs();
            foreach (long value in valuesToDecrypt)
            {
                long powmod = PowMod(value, privateKey.Key, privateKey.Product);
                sb.Append((char)powmod);
            }
            return sb.ToString();
        }
        /// <summary>
        /// Encrypt a string using the public key
        /// </summary>
        /// <param name="stringToEncrypt">The string to encrypt</param>
        /// <param name="publicKey">The public key</param>
        /// <returns>The encrypted string</returns>
        public string Encrypt(string stringToEncrypt, RsaKey publicKey)
        {
            long[] encryptedValues = new long[stringToEncrypt.Length];
            int i = 0;
            foreach (char c in stringToEncrypt)
            {
                encryptedValues[i] = PowMod(c, publicKey.Key, publicKey.Product);
                i++;
            }
            return Convert.ToBase64String(encryptedValues.GetBytes());
        }

        // Modular calculation
        private long PowMod(long a, long b, long n)
        {
            long x = 1, y = a;

            while (b > 0)
            {
                if (b % 2 == 1)
                    x = (x * y) % n;
                y = (y * y) % n; // Squaring the base
                b /= 2;
            }

            return x % n;
        }

        // Find the decryption key of e and k
        private long FindDecryptionKey(long e, long k)
        {
            for (long d = 1; d < k * 2; d++)
            {
                if (d * e % k == 1) return d;
            }
            throw new Exception("Something went horribly wrong");
        }

        // Generate a random Int64 number
        private long GenerateRandomLong(long min, long max, Random rand)
        {
            long result = rand.Next((int)(min >> 32), (int)(max >> 32));
            result <<= 32;
            result |= (long)rand.Next((int)min, (int)max);
            return result;
        }

        // Generate a random prime number
        private long GenerateRandomPrimeNumber()
        {
            Random random = new Random();
            while (true)
            {
                long number = GenerateRandomLong((long)Math.Pow(10, 3), (long)Math.Pow(10, 4), random);
                if (IsPrime(number))
                    return number;
            }
        }

        // Checks if a number is prime
        private bool IsPrime(long number)
        {
            if (number <= 1) return false;
            if (number == 2) return true;
            if (number % 2 == 0) return false;

            long boundary = (long)Math.Floor(Math.Sqrt(number));

            for (long i = 3; i <= boundary; i += 2)
                if (number % i == 0)
                    return false;

            return true;
        }

        // Find random numbers that are coprime to eachother
        private long FindDerivedNumber(long k)
        {
            Random random = new Random();
            while (true)
            {
                long rnd = GenerateRandomLong(2, Math.Abs(k), random);
                if (AreCoprime(rnd, k))
                    return rnd;
            }
        }

        // Find the greatest common divisor
        private long Gcd(long a, long b)
        {
            long t;
            while (b != 0)
            {
                t = a;
                a = b;
                b = t % b;
            }
            return a;
        }

        // Check if two values are coprime
        private bool AreCoprime(long a, long b)
        {
            return Gcd(a, b) == 1;
        }
    }
}
