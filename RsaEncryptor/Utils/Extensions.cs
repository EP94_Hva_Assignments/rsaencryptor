﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace RsaEncryptor.Utils
{
    public static class Extensions
    {
        public static byte[] GetBytes(this long[] values)
        {
            byte[] converted = new byte[values.Length * 8];
            for (int i = 0; i < values.Length; i++)
            {
                byte[] bytes = BitConverter.GetBytes(values[i]);
                for (int j = 0; j < bytes.Length; j++)
                {
                    converted[i * 8 + j] = bytes[j];
                }
            }
            return converted;
        }

        public static long[] GetLongs(this byte[] values)
        {
            long[] converted = new long[values.Length / 8];
            for (int i = 0; i < values.Length; i += 8)
                converted[i / 8] = BitConverter.ToInt64(values, i);
            return converted;
        }
    }
}
