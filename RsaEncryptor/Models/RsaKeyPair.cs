﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RsaEncryptor.Models
{
    public class RsaKeyPair
    {
        public RsaKey PublicKey { get; set; }
        public RsaKey PrivateKey { get; set; }
        public long K { get; set; }
    }
}
