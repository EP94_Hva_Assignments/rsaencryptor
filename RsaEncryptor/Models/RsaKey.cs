﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RsaEncryptor.Models
{
    public class RsaKey
    {
        public long Key { get; set; }
        public long Product { get; set; }
    }
}
