using NUnit.Framework;
using RsaEncryptor.Models;
using System;

namespace RsaEncryptor.Tests
{
    public class Tests
    {
        private Rsa _rsaEncryptor;
        [SetUp]
        public void Setup()
        {
            _rsaEncryptor = new Rsa();
        }

        [Test]
        public void TestEncryptionKey()
        {
            RsaKeyPair keyPair = _rsaEncryptor.GenerateRsaKeyPair();
            Assert.IsTrue(Gcd(keyPair.PublicKey.Key, keyPair.K) == 1);
        }

        [Test]
        public void TestDecryptionKey()
        {
            RsaKeyPair keyPair = _rsaEncryptor.GenerateRsaKeyPair();
            Assert.IsTrue((keyPair.PrivateKey.Key * keyPair.PublicKey.Key) % keyPair.K == 1);
        }

        [Test]
        public void TestEncryptDecrypt()
        {
            RsaKeyPair keyPair = _rsaEncryptor.GenerateRsaKeyPair();
            string stringToEncrypt = "TestStringToEncrypt";
            string encrypted = _rsaEncryptor.Encrypt(stringToEncrypt, keyPair.PublicKey);
            string decrypted = _rsaEncryptor.Decrypt(encrypted, keyPair.PrivateKey);
            Assert.AreEqual(stringToEncrypt, decrypted);
        }

        private long Gcd(long a, long b)
        {
            long t;
            while (b != 0)
            {
                t = a;
                a = b;
                b = t % b;
            }
            return a;
        }
    }
}