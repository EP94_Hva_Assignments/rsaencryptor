#RsaEncryptor
In the folder "Deliverables" there is an executable. Run the executable to encrypt and decrypt strings with the RSA algorithm.

To run the tests, Visual Studio and the .Net Core 3.1 SDK is required. If installed, doubleclick on the RsaEncryptor.sln file to open.
When opened, right press on "RsaEncryptor.Tests" and click "Run tests";